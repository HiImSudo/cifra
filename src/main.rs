extern crate clap;

mod cifra;
mod utils;

use std::fs::File;
use std::io::Read;
use std::thread;
use std::collections::{HashMap, HashSet};
use std::env;

use std::time::{Duration, Instant};
use std::thread::sleep;

use cifra::transformations::{Direction, transform, transform_word};
use cifra::analysis::Analyser;
use cifra::analysis::Timer;

use utils::conversions::{string_to_chars};

use clap::{Arg, App, SubCommand};

fn main() {
    let execution = Timer::new("Application Execution");

    let matches = App::new("cifra")
        .version("1.0")
        .author("A Helberg;CA Du Plooy")
        .about("A caesar cipher solver implemented using pure rust.")
        .arg(Arg::with_name("dictionary")
            .short("d")
            .long("dictionary")
            .value_name("dictionary")
            .help("Path to the dictionary file ( newline seperated )")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("cipher")
            .short("c")
            .long("cipher")
            .value_name("cipher")
            .help("Path to the cipher file ( newline seperated )")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("alphabet")
            .short("a")
            .long("alphabet")
            .value_name("alphabet")
            .help("The alphabet to use")
            .takes_value(true)
            .required(false))    
        .arg(Arg::with_name("sample")
            .short("sample")
            .long("sample")
            .value_name("sample")
            .help("Use only n values of input")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("time")
            .short("t")
            .long("time")
            .value_name("time")
            .help("print benchmarking data")
            .takes_value(false)
            .required(false))
        .arg(Arg::with_name("frequency-char")
            .short("f")
            .long("frequency-char")
            .value_name("frequency-char")
            .help("The most common character in target language")
            .takes_value(true)
            .required(false))
        .arg(Arg::with_name("out")
            .short("o")
            .long("out")
            .value_name("out")
            .help("Output result to stdout")
            .takes_value(false)
            .required(false))
        .get_matches();

    // ./cifra cipher.txt dictionary.txt 
    let config_timer = Timer::new("Parsing CLI Args");
    let cipher_file: String = matches.value_of("cipher").unwrap_or("./caesar.txt").to_string();
    let dictionary_file = matches.value_of("dictionary").unwrap_or("./dictionary.txt").to_string();
    let alphabet: String = matches.value_of("alphabet").unwrap_or("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").to_string(); 
    let alphabet_clone = alphabet.clone();
    let display: bool = matches.is_present("out");
    let sample_str = matches.value_of("sample").unwrap_or("0").to_string();
    let sample: u64 = sample_str.parse().expect("Couldn't parse samples");

    let time: bool = matches.is_present("time");
    let mfc_str: String = matches.value_of("frequency-char").unwrap_or("e").to_string();
    let mfc: char = mfc_str.into_bytes()[0] as char;

    if time {
        config_timer.stop();
    }
    // Read the cipher text (needs to be finished immediately)

    let thread_spawn = Timer::new("Spawning a thread");
    let cipher_handle = thread::spawn(move || {
        let cipher_read = Timer::new("Reading ciphertext from file && Map operations");

        let mut file = File::open(cipher_file).expect("Could not find the cipher! ");
        let mut buf = String::new();

        file.read_to_string(&mut buf).expect("Could not read from the cipher file!");
        let data: Vec<u8> = buf.clone().into_bytes();
        let mut words: Vec<String> = Vec::new();
        
        for word in buf.split(" "){ // todo: Split on any ignored value
            words.push(word.to_string());
        }

        let mut map: HashMap<char, u64> = HashMap::new();
        for u in data.clone(){
            let c: char = u as char;
            *map.entry(c).or_insert(1) += 1;
        }

        let mut count_vec: Vec<(&char, &u64)> = map.iter().collect();
        count_vec.sort_by(|a, b| b.1.cmp(a.1));

        let mut ans:Vec<(char, u64)> = Vec::new();
        for x in count_vec {
            ans.push((*x.0, *x.1));
        }

        if time {
            cipher_read.stop();
        }

        (buf, words, ans)
    });

    if time {
        thread_spawn.stop();
    }


    let dictionary_handle = thread::spawn(move || {
        let dictionary_timer = Timer::new("Dictionary read && Map Operations");

        let mut file = File::open(dictionary_file).expect("Could not find the dictionary! ");
        let mut buf = String::new();

        file.read_to_string(&mut buf).expect("Could not read from the dictionary file!");
        let mut set: HashSet<String> = HashSet::new();

        for word in buf.split("\n"){
            set.insert(word.to_string());
        }
        
        if time {
            dictionary_timer.stop();
        }

        set
    });

    let (buf, words, count_vec) = cipher_handle.join().expect("Failed to get return from cipher thread!");

    let dictionary = dictionary_handle.join().expect("Failed to get return from dictionary thread!");

    // Verify
    let analyse_timer = Timer::new("Analysis of ciphertext && solution");
    let mut analyser = Analyser::new(count_vec, vec!['.',' ',';','!','?',',',':', '\r', '\n', '\t'], dictionary, alphabet, mfc);
    analyser.sampling(sample);

    let shifts = analyser.start(words);
    if time {
        analyse_timer.stop();
    }

    if display{
        let mut highest = 0.0;
        let mut most_probable_shift = 0;
        for shift in shifts.clone(){
            if shift.1 > highest{
                highest = shift.1;
                most_probable_shift = shift.0;
            }
        }

        if highest <= 25.0 {
            println!("---------------------------------------------\n{}\n---------------------------------------------\n","WARNING: The highest probable answer has a very low match rate and may be incorrect!\n");
        }

        let result: String = transform(most_probable_shift as u8, Direction::LEFT, string_to_chars(buf), string_to_chars(alphabet_clone), vec!['.',' ',';','!','?',',',':', '\r', '\n', '\t']).into_iter().collect();                       
        println!("{}", result);
    }

    println!("---------------------------------------------\n{:?}\n---------------------------------------------" , shifts);

    if time {
        execution.stop();
    }
}
