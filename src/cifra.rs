pub mod transformations{
    use std::collections::HashSet;
    #[derive(PartialEq)]
    pub enum Direction {
        LEFT,
        RIGHT,
    }

    pub fn string_to_chars(buffer: String) -> Vec<char>{
            let mut buff: Vec<char> = Vec::new();
            for i in buffer.into_bytes(){
                buff.push(i as char);   // TODO: PERF
            }
            buff
    }

    pub fn transform(shiftValue: u8, direction: Direction, stringToShift: Vec<char>, 
    alphabet: Vec<char>, ignoreAlphabet: Vec<char>) -> Vec<char> {
        let mut newString: Vec<char> = Vec::new();

        for x in stringToShift {
            if !ignoreAlphabet.contains(&x) {
                if direction == Direction::RIGHT {
                    let mVal = alphabet.iter().position(|&y| y == x);
                    let mut xValue = 0;

                    match mVal{
                        Some(n)=>{
                            xValue = n;
                        }
                        None=>{
                            continue;
                        }
                    } 
                    let shift = xValue as u8 + shiftValue;
                    //Case 1 : The is a overflow
                    if shift > (alphabet.len() as u8 - 1) {
                        let index = shift as usize % alphabet.len();
                        newString.push(alphabet[index as usize]);
                    }
                    //Case 2 : There is no overflow
                    else {
                        newString.push(alphabet[shift as usize]);
                    }
                }
                else {
                    let mVal = alphabet.iter().position(|&y| y == x);
                    let mut xValue = 0;

                    match mVal {
                        Some(n) => {
                            // println!("n is {}", n);
                            xValue = n;
                            }
                        None => {continue}
                    }
                    let shift: i8 = xValue as i8 - shiftValue as i8;
                    //Case 3 : The is an underflow
                    if shift < 0 {
                        let index = (alphabet.len()-1) as i8 + shift;
                        newString.push(alphabet[(index+1) as usize]);
                    }
                    //Case 4 : There is no overflow
                    else {
                        newString.push(alphabet[shift as usize]);
                    }
                }
            }
            else {
                newString.push(x);
            }
        }

        newString

    }

    pub fn transform_word(shiftValue: u8, direction: Direction, stringToShift: String, alphabet: Vec<char>, ignoreAlphabet: Vec<char>) -> String{
        let word = string_to_chars(stringToShift);
        transform(shiftValue, direction, word, alphabet, ignoreAlphabet).into_iter().collect()
    }
}

pub mod analysis{
    use std::collections::{HashMap, HashSet};
    use {transform, transform_word, Direction, string_to_chars};
    use std::time::{Instant};
    pub struct Timer{
        start: Instant,
        message: String,
    }

    impl Timer{
        pub fn new(message: &str) -> Timer {
            Timer {
                start: Instant::now(),
                message: message.to_string(),
            }
        }

        pub fn stop(&self){
            println!("Operation: {} took {:?}", self.message, Instant::now().duration_since(self.start));
            drop(self);
        }
    }
    
    pub struct Analyser{
        ignore: Vec<char>,
        valid_words: HashSet<String>,
        freq_map: Vec<(char, u64)>,
        alphabet: String,
        sample: u64,
        most_frequent_char: char,
    }

    impl Analyser{
        pub fn new(freq_map: Vec<(char, u64)>, ignore: Vec<char>, valid_words: HashSet<String>, alphabet: String, mfc: char) -> Analyser{
            let length = valid_words.len();
            Analyser{
                ignore: ignore,
                valid_words: valid_words,
                freq_map: freq_map,
                alphabet: alphabet,
                sample: length as u64,
                most_frequent_char: mfc,
            }
        }

        pub fn sampling(&mut self, sample: u64){
            self.sample = sample;
        }
        
        pub fn start(&self, data: Vec<String>) -> Vec<(i16, f32)>{
            let sample_size: f32 = 
                if self.sample != 0 {
                    if self.sample > data.len() as u64{
                        data.len() as f32
                    }else{
                        self.sample as f32
                    }
                }else{
                    data.len() as f32
                };

            let mut results: Vec<(i16, f32)> = Vec::new();
            let mut j = 0;
            let mut i = 0;

            while j < 6 {
                if self.ignore.contains(&self.freq_map[i].0){
                    j = j - 1;
                }else{
                    let shift: u8;
                    if  self.most_frequent_char as u8 > self.freq_map[i].0 as u8{
                            shift = self.most_frequent_char as u8 - self.freq_map[i].0 as u8;
                    }else{
                            shift = self.freq_map[i].0 as u8 - self.most_frequent_char as u8 ;
                    }

                    // for each possible shift value
                    // return an array containing said shift
                    // with it's accuracy

                    let mut correct = 0;
                    let mut n = 1;
                    for word in &data{
                        // Shift every word with the shift function.
                        let new_word: String = transform(shift as u8, Direction::LEFT, string_to_chars(word.clone()), string_to_chars(self.alphabet.clone()), self.ignore.clone()).into_iter().collect();                       
                        if self.valid_words.contains(&new_word){
                            correct = correct + 1;
                        }

                        if n-1 == self.sample && self.sample != 0{
                            break;   
                        }

                        n = n + 1;
                    }

                    results.push((shift as i16, (correct as f32)/sample_size  * 100.0));                        
                    
                    j = j + 1;
                }
                i = i + 1;
            }
            results
        }
    }
    
}